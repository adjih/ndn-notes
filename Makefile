NAME=table-eval

all: ${NAME}.png

${NAME}.png: ${NAME}.pdf Makefile
	convert -define pdf:use-trimbox=true -density 150 ${NAME}.pdf -quality 90 ${NAME}-raw.png
	convert -background white -alpha remove -trim ${NAME}-raw.png ${NAME}.png

${NAME}.pdf: ${NAME}.tex Makefile
	pdflatex ${NAME}

